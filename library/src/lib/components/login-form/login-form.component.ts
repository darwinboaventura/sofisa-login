import {Component, EventEmitter, Output} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '@sofisa/core';

@Component({
	selector: 'so-login-form',
	templateUrl: './login-form.component.html',
	styleUrls: ['./login-form.component.scss']
})

export class LoginFormComponent {
	@Output() wasSubmited: EventEmitter<any> = new EventEmitter();
	
	state: any = {
		form: {
			formGroup: this.fb.group({
				username: [null, Validators.required],
				password: [null, Validators.required]
			}),
			validations: {
				username: {
					type: '',
					message: ''
				},
				password: {
					type: '',
					message: ''
				}
			},
			wasSubmitted: false
		}
	};
	
	constructor(public fb: FormBuilder) {}
	
	updateFieldValue(value: any, name: any) {
		if (value) {
			this.state.form.formGroup.get(name).setValue(value);
			
			this.state.form.validations[name].type = '';
			this.state.form.validations[name].message = '';
		} else if (this.state.form.wasSubmitted) {
			this.state.form.validations[name].type = 'error';
			this.state.form.validations[name].message = 'Campo obrigatório';
		}
	}
	
	handleForm(e: any) {
		this.state.form.wasSubmitted = true;
		
		if (this.state.form.formGroup.valid) {
			this.wasSubmited.emit(this.state.form.formGroup.value);
		}
	}
}
