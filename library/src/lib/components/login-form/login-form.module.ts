import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginFormComponent} from './login-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputModule, ButtonModule, CheckboxModule} from '@sofisa/ui';

@NgModule({
	declarations: [LoginFormComponent],
	imports: [
		CommonModule,
		InputModule,
		ButtonModule,
		CheckboxModule,
		ReactiveFormsModule
	],
	exports: [LoginFormComponent]
})

export class LoginFormModule { }
